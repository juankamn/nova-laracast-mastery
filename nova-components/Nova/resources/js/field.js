Nova.booting((Vue, router, store) => {
  Vue.component('index-nova', require('./components/IndexField'))
  Vue.component('detail-nova', require('./components/DetailField'))
  Vue.component('form-nova', require('./components/FormField'))
})
