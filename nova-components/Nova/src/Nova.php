<?php

namespace Vendor\Nova;

use Laravel\Nova\Fields\Field;

class Nova extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'nova';
}
