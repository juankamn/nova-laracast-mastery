<?php

namespace App\Nova\Metrics;

use App\Models\Post;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Metrics\Trend;
use Laravel\Nova\Metrics\TrendResult;

class PostPerDay extends Trend
{
    public $name = 'Post Per Month';

    public function name()
    {
        return 'Post por mes';
    }

    /**
     * Calculate the value of the metric.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return mixed
     */
    public function calculate(NovaRequest $request)
    {
//        return $this->countByDays($request, Post::class);
//        return $this->countByWeeks($request, Post::class);
//        return $this->countByDays($request, Post::class);
//        return $this->countByHours($request, Post::class);
//        return $this->countByMinutes($request, Post::class);
//        return $this->averageByMonths($request, Post::class,'word_count');
//        return $this->minByMonths($request, Post::class,'word_count');
//        return $this->maxByMonths($request, Post::class,'word_count');
//        return $this->countByMonths($request, Post::class)->showLatestValue();
//        return $this->countByMonths($request, Post::class)->showSumValue();

        return (new TrendResult)->trend([
            'Day 1' => 20,
            'Day 2' => 45,
            'Day 3' => 40,
        ]);
    }

    /**
     * Get the ranges available for the metric.
     *
     * @return array
     */
    public function ranges()
    {
        return [
//            30 => __('30 Days'),
//            60 => __('60 Days'),
//            90 => __('90 Days'),
            6 => __('6 Months'),
            12 => __('12 Months'),
            18 => __('18 Months'),
        ];
    }

    /**
     * Determine for how many minutes the metric should be cached.
     *
     * @return  \DateTimeInterface|\DateInterval|float|int
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'post-per-day';
    }
}
